package com.example.sorteioapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

import static android.os.Build.VERSION_CODES.O;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Random r = new Random();

        Button novoS = (Button) findViewById(R.id.button);
        novoS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int numSorteado = 0;
                EditText num1 = findViewById(R.id.editText1);
                EditText num2 = findViewById(R.id.editText2);


                String n1 = num1.getText().toString();
                String n2 = num2.getText().toString();

                int numero1 = Integer.parseInt(n1);
                int numero2 = Integer.parseInt(n2);


                Random random = new Random();

                numSorteado = r.nextInt((numero2 - numero1) + 1) + numero1;
                String texto = Integer.toString(numSorteado);

                TextView re = (TextView) findViewById(R.id.textView);
                re.setText(texto);

            }
        });

    }
}
